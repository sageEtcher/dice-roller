#include "io_control.h"

int
main ( int argc , char** argv )
{
    Settings user_settings ;
    unsigned long int total , i , recent_roll ;

    init_random () ;
    user_settings = get_settings ( argv , argc ) ;

    if ( user_settings.help == _TRUE_ )
    {
        display_help_screen () ;
        return 0 ;
    }

    total = 0 ;
    for ( i = 0 ; i < user_settings.dice_count ; i ++ )
    {
        recent_roll = roll_single ( user_settings.dice_sides ) ;
        total += recent_roll ;

        if ( user_settings.verbose == _TRUE_ )
        {
            printf ( "Roll #%lu\t%lu\n" , i , recent_roll ) ;
        }
    }

    if ( user_settings.average == _TRUE_ )
        printf ( "Average Roll\t%lu\n" , total / user_settings.dice_count ) ;

    if ( user_settings.sum == _TRUE_ )
        printf ( "Total       \t%lu\n" , total ) ;

	return 0 ;
}
