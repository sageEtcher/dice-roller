#include "io_control.h"

Settings
default_settings ( void )
{
    Settings user_settings ;

    /* roll 1d20 */
    user_settings.dice_sides = 20 ;
    user_settings.dice_count = 1 ;

    /* show the roll */
    user_settings.verbose   = _TRUE_ ;
    user_settings.help      = _FALSE_ ;

    /* and just the roll */
    user_settings.average   = _FALSE_ ;
    user_settings.sum       = _FALSE_ ;

    /* to console */
    user_settings.output_type = _CONSOLE_OUTPUT_ ;

    return user_settings ;
}

unsigned long int
extract_ulong_from_substring ( char *sub_string )
{
    unsigned long int   new_number ;

    new_number = 0 ;
    while ( isdigit ( *sub_string ) )
    {
        new_number *= 10 ;
        new_number += (int)*sub_string++ - (int)'0' ;
    }

    return new_number ;
}


void
process_input_arguments ( Settings *default_settings , char *argument_string )
{
    char    *rcp ;

    /* check for verbose flag */
    if ( ( rcp = strstr ( argument_string , "-v" ) )        != NULL ||
         ( rcp = strstr ( argument_string , "-verbose" ) )  != NULL )
    {
        (*default_settings).verbose = _TRUE_ ;
    }

    /* check for sum flag */
    if ( ( rcp = strstr ( argument_string , "-s" )  )       != NULL ||
         ( rcp = strstr ( argument_string , "-sum" ) )      != NULL )
    {
        (*default_settings).sum = _TRUE_ ;
    }

    /* check for average flag */
    if ( ( rcp = strstr ( argument_string , "-a" ) )        != NULL ||
         ( rcp = strstr ( argument_string , "-average" ) )  != NULL )
    {
        (*default_settings).average = _TRUE_ ;
    }

    /* check for help flag */
    if ( ( rcp = strstr ( argument_string , "-h" ) )        != NULL ||
         ( rcp = strstr ( argument_string , "-help" ) )     != NULL )
    {
        (*default_settings).help = _TRUE_ ;
    }

    /* get the number of dice rolled */

    if ( ( rcp = strstr ( argument_string , "-n" ) )        != NULL )
    {
        (*default_settings).dice_count = extract_ulong_from_substring (
            rcp += strlen ( "-n" ) * sizeof (char) ) ;
    }

    if ( ( rcp = strstr ( argument_string , "-d" ) )        != NULL )
    {
        (*default_settings).dice_sides = extract_ulong_from_substring (
            rcp += strlen ( "-d" ) * sizeof (char) );
    }

    return ;
}


Settings
get_settings ( char **argument_list , int argument_count )
{
    Settings user_settings ;
    char    *argument_string ;

    /* convert argument char** to a single char* for regex */
    argument_string = string_array_to_string ( argument_list , argument_count ) ;

    /* start with default settings */
    user_settings = default_settings () ;

    /* process settings out of the suplied arguments */
    process_input_arguments ( &user_settings , argument_string ) ;

    free ( argument_string ) ;
    return user_settings ;
}


char*
string_array_to_string ( char** string_array , int count )
{
    char    *return_string , *item ;
    int     return_string_count , i ;

    /* get total length for return string */
    return_string_count = 0 ;
    for ( i = 0 ; i < count ; i ++ )
        return_string_count += strlen ( string_array[i] ) ;

    /* allocate needed space */
    return_string = (char*)calloc ( sizeof (char) , return_string_count ) ;

    /* concatenate all elements of string_array to return_string */
    for ( i = 0 ; i < count ; i ++ )
        strcat ( return_string , string_array[i] ) ;

    return return_string ;
}



