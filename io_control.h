#ifndef IO_HEADER
#define IO_HEADER

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef int Bool ;
#define _TRUE_      1
#define _FALSE_     0

typedef int Output ;
#define _NO_OUTPUT_         0
#define _CONSOLE_OUTPUT_    1
#define _FILE_OUTPUT_       2


typedef struct
{
    unsigned long int   dice_sides , dice_count ;
    Bool    verbose , help;
    Bool    average , sum ;
    Output  output_type ;
    char    *output_filename ;
} Settings ;


Settings    default_settings ( void ) ;
Settings    get_settings ( char **argument_list , int argument_count ) ;

char*       string_array_to_string ( char **string_array , int count ) ;



#endif
